﻿public class Student
{
    public int Id;
    public string Name;
    public int Age;
    public double GPA;
    public static Student[] students = new Student[2];

    public Student()
    {

    }
    public Student(int _id, string _Name, int _age, double _gpa)
    {


    }

    public void ExistStudent()
    {
        students[0] = new Student();
        students[0].Id = 1;
        students[0].Name = "Khanh";
        students[0].Age = 22;
        students[0].GPA = 10;

        students[1] = new Student();
        students[1].Id = 2;
        students[1].Name = "Khanh";
        students[1].Age = 22;
        students[1].GPA = 10;
    }

    public Student AddNewStudent()
    {
        int newElements = students.Length + 1;
        Array.Resize(ref students, newElements);
        Student newStudent = new Student();
        Console.WriteLine("Enter student id: ");
        newStudent.Id = int.Parse(Console.ReadLine());
        Console.WriteLine("Enter student name: ");
        newStudent.Name = Console.ReadLine();
        Console.WriteLine("Enter student age: ");
        newStudent.Age = int.Parse(Console.ReadLine());
        Console.WriteLine("Enter student gpa: ");
        newStudent.GPA = double.Parse(Console.ReadLine());
        return students[newElements - 1] = newStudent;
    }

    public void PrintAllStudents()
    {
        foreach (Student student in students)
        {
            Console.WriteLine("Id: " + student.Id + "| Tên: " + student.Name + "| Tuổi: " + student.Age + "| GPA: " + student.GPA);
        }
    }

    public string CheckGPA(int studentId)
    {
        foreach (Student student in students)
        {
            if (student.Id == studentId)
            {
                Console.WriteLine("Id: " + student.Id + "| Tên: " + student.Name + "| Tuổi: " + student.Age + "| GPA: " + student.GPA);
                if (student.GPA >= 8 && student.GPA <= 10)
                {
                    return "Giỏi";
                }
                else if (student.GPA >= 6.5 && student.GPA < 8)
                {
                    return "Khá";
                }
                else if (student.GPA > 5 && student.GPA < 6.5)
                {
                    return "Trung bình";
                }
                else
                {
                    return "Yếu";
                }
            }
        }
        return "Không tìm thấy học sinh";
    }


}