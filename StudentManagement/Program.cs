﻿Console.OutputEncoding = System.Text.Encoding.UTF8;
Student student = new Student();
void Menu()
{
    Console.WriteLine("-----Students Management-----");
    Console.WriteLine("|1: Nhập thông tin sinh viên|");
    Console.WriteLine("|2: In thông tin sinh viên  |");
    Console.WriteLine("|3: Xếp loại sinh viên      |");
    Console.WriteLine("-----------------------------");
}
int choice = 0;
do
{
    student.ExistStudent();
    Menu();
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            student.AddNewStudent();
            break;
        case 2:
            student.PrintAllStudents();
            break;
        case 3:
            Console.WriteLine("Enter Student ID: ");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine(student.CheckGPA(id));
            break;
        default:
            break;
    }

} while (choice <= 3);