﻿class Circle
{
    public double Radius;

    public static Circle[] circles = new Circle[11];

    public Circle()
    {
        Radius = 3.5;
    }


    public Circle(double _radius)
    {
        Radius = _radius;
    }

    public double GetArea()
    {
        return Math.Round(Math.PI * Math.Pow(Radius, 2), 2);
    }

    public double GetCircumference()
    {
        return Math.Round(2 * Math.PI * Radius, 2);
    }
    public static void GetAreaAndCircumference()
    {
        for (int i = 1; i < Circle.circles.Length; i++)
        {
            Circle.circles[i] = new Circle(i);
            Console.WriteLine("Circle No." + i + " | Area: " + Circle.circles[i].GetArea() + " | Circumference: " + Circle.circles[i].GetCircumference());
        }
    }
}