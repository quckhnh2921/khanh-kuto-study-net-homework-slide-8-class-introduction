﻿int choice = 0;
Employee employee = new Employee();
void Menu()
{
    Console.WriteLine("-------Employee Management-------");
    Console.WriteLine("|  1: Print all employee        |");
    Console.WriteLine("|  2: Add new employee          |");
    Console.WriteLine("|  3: Remove employee by name   |");
    Console.WriteLine("|  4: Exit                      |");
    Console.WriteLine("---------------------------------");
}

do
{
    employee.ExistEmployee();
    Menu();
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            employee.PrintAllEmployee();
            break;
        case 2:
            employee.AddNewEmployee();
            Console.WriteLine("Add sucessful !");
            break;
        case 3:
            Console.Write("Enter name to remove: ");
            string name = Console.ReadLine();
            employee.RemoveEmployeeByName(name);
            Console.WriteLine("Remove sucessful !");
            break;
        default:
            Console.WriteLine("BYE BYE !");
            break;
    }
} while (choice < 4);