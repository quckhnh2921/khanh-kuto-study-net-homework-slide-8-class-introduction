﻿class Employee
{
    public string Name;
    public int Age;
    public string Position;
    public double Salary;
    public static Employee[] employees = new Employee[2];

    public Employee()
    {

    }

    public void ExistEmployee()
    {
        employees[0] = new Employee();
        employees[0].Name = "Khanh";
        employees[0].Age = 20;
        employees[0].Position = "PM";
        employees[0].Salary = 5000;

        employees[1] = new Employee();
        employees[1].Name = "B";
        employees[1].Age = 20;
        employees[1].Position = "PM";
        employees[1].Salary = 5000;
    }
    public void PrintAllEmployee()
    {
        foreach (Employee e in employees)
        {
            Console.WriteLine("Name: " + e.Name + " | Age: " + e.Age + " | Position: " + e.Position + " | Salary: " + e.Salary);
        }
    }

    public Employee AddNewEmployee()
    {
        int newSizeArray = employees.Length + 1;
        Array.Resize(ref employees, newSizeArray);
        Employee newEmployee = new Employee();
        Console.WriteLine("Enter employee name: ");
        newEmployee.Name = Console.ReadLine();
        Console.WriteLine("Enter employee age: ");
        newEmployee.Age = int.Parse(Console.ReadLine());
        Console.WriteLine("Enter employee position: ");
        newEmployee.Position = Console.ReadLine();
        Console.WriteLine("Enter salary: ");
        newEmployee.Salary = double.Parse(Console.ReadLine());
        PrintEmployeeInfo(newEmployee);
        return employees[newSizeArray - 1] = newEmployee;
    }

    public void PrintEmployeeInfo(Employee e)
    {
        Console.WriteLine("Name: " + e.Name + " | Age: " + e.Age + " | Position: " + e.Position + " | Salary: " + e.Salary);
    }
    public void RemoveEmployeeByName(string name)
    {
        for (int i = 0; i < employees.Length; i++)
        {
            if (employees[i].Name == name)
            {
                employees[i] = null;
                Array.Resize(ref employees, employees.Length - 1);
            }
        }
    }
}