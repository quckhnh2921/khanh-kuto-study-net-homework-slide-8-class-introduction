﻿class Student
{
    public string Name;
    public int Age;
    public int Grade;

    public Student()
    {
        Name = "John Wick";
        Age = 15;
        Grade = 9;
    }

    public void GetInfo()
    {
        Console.WriteLine("Name: " + Name + " | Age: " + Age + " | Grade: " + Grade);
    }

    public Student SetGrade(Student student)
    {
        student.Grade = 10;
        return student;
    }
}