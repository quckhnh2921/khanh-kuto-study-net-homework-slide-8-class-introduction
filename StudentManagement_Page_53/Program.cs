﻿using System.Xml.Serialization;

Student student = new Student();
int choice = 0;
void Menu()
{
    Console.WriteLine("------Menu------");
    Console.WriteLine("| 1: Get Info  |");
    Console.WriteLine("| 2: Set grade |");
    Console.WriteLine("| 3: Exit      |");
    Console.WriteLine("----------------");
}

do
{
    Menu();
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            student.GetInfo();
            break;
        case 2:
            student.SetGrade(student);
            student.GetInfo();
            break;
        default:
            Console.WriteLine("BYE BYE !");
            break;
    }
} while (choice < 3);