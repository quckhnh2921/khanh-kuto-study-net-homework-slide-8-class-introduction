﻿class Employee
{
    public int Id;
    public string Name;
    public int Age;
    public double Salary;

    public Employee()
    {
        Id = 0;
        Name = "Unknown";
        Age = 20;
        Salary = 5000.0;
    }

    public Employee(int _id, string _name)
    {
        Id = _id;
        Name = _name;
    }

    public Employee(int _id, string _name, int _age, double _salary)
    {
        Id = _id;
        Name = _name;
        Age = _age;
        Salary = _salary;
    }

    public void DisplayEmployee(Employee employee)
    {
        Console.WriteLine("| Id: " + employee.Id + " | Name: " + employee.Name + " | Age: " + employee.Age + " | Salary: " + employee.Salary);
    }
}