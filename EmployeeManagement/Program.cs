﻿Employee firstEmployee = new Employee();
firstEmployee.DisplayEmployee(firstEmployee);

Employee secondEmployee = new Employee(1, "Khanh");
secondEmployee.DisplayEmployee(secondEmployee);

Employee thirdEmployee = new Employee(2, "Khanh Depzai", 20, 10000);
thirdEmployee.DisplayEmployee(thirdEmployee);