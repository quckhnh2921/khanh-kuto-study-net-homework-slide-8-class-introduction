﻿class Games
{
    public void OresToGold()
    {
        const int PRICE_OF_FIRST_TEN_ORES = 10;
        const int PRICE_OF_NEXT_FIVE_ORES = 5;
        const int PRICE_OF_NEXT_THREE_ORES = 2;
        const int PRICE_OF_REMAINDER = 1;

        int CheckOreAndTurnToGold(int numberOfOre)
        {
            //Get 10 ores and turn it to price
            int firstTenOres = Math.Min(numberOfOre, 10);
            int firstTenOresToGold = firstTenOres * PRICE_OF_FIRST_TEN_ORES;
            numberOfOre -= firstTenOres;

            //Get next 5 ores and turn it to price
            int nextFiveOres = Math.Min(numberOfOre, 5);
            int nextFiveOresToGold = nextFiveOres * PRICE_OF_NEXT_FIVE_ORES;
            numberOfOre -= nextFiveOres;

            //Get next 3 ores and turn it to price
            int nextThreeOres = Math.Min(numberOfOre, 3);
            int nextThreeOresToGold = nextThreeOres * PRICE_OF_NEXT_THREE_ORES;
            numberOfOre -= nextThreeOres;

            //Get the remainder and turn it to price
            int remainder = numberOfOre;
            int nextRemainderToGold = numberOfOre * PRICE_OF_REMAINDER;

            //Total price
            int totalGold = firstTenOresToGold + nextFiveOresToGold + nextThreeOresToGold + nextRemainderToGold;

            return totalGold;
        }

        Console.Write("Enter number of your ores: ");
        int numberOfOre = int.Parse(Console.ReadLine());

        Console.WriteLine("You sell " + numberOfOre + " ores and get " + CheckOreAndTurnToGold(numberOfOre) + " gold !");
    }

    public void RockScissorPaper()
    {
        const int KEO = 1;
        const int BUA = 2;
        const int BAO = 3;
        const int DRAW = 0;
        const int PLAYER_ONE_WIN = 1;
        const int PLAYER_TWO_WIN = 2;

        //game's rule
        int Rule(int playerOneChoose, int playerTwoChoose)
        {
            if (playerOneChoose == KEO && playerTwoChoose == BAO || playerOneChoose == BUA && playerTwoChoose == KEO || playerOneChoose == BAO && playerTwoChoose == BUA)
            {
                return PLAYER_ONE_WIN;
            }
            else if (playerTwoChoose == KEO && playerOneChoose == BAO || playerTwoChoose == BUA && playerOneChoose == KEO || playerTwoChoose == BAO && playerOneChoose == BUA)
            {
                return PLAYER_TWO_WIN;
            }
            else
            {
                return DRAW;
            }
        }
        //game's result
        string CheckResult(int result)
        {
            if (result == PLAYER_ONE_WIN)
            {
                return "Player 1 win !";
            }
            else if (result == PLAYER_TWO_WIN)
            {
                return "Player 2 win !";
            }
            else
            {
                return "Draw";
            }
        }

        void DisplayChoose()
        {
            Console.WriteLine("KEO : 1");
            Console.WriteLine("BUA : 2");
            Console.WriteLine("BAO : 3");
        }

        DisplayChoose();
        //player input their choice
        Console.WriteLine("Player 1 choose: ");
        int playerOneChoose = int.Parse(Console.ReadLine());
        Console.WriteLine("Player 2 choose: ");
        int playerTwoChoose = int.Parse(Console.ReadLine());
        //print the result
        int checkResult = Rule(playerOneChoose, playerTwoChoose);
        Console.WriteLine(CheckResult(checkResult));

    }
}