﻿Games game = new Games();
int choice = 0;
void Menu()
{
    Console.WriteLine("----------Games---------");
    Console.WriteLine("| 1: Ores to gold      |");
    Console.WriteLine("| 2: Rock Scissor Paper|");
    Console.WriteLine("| 3: Exit              |");
    Console.WriteLine("------------------------");
}
do
{
    Menu();
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            game.OresToGold();
            break;
        case 2:
            game.RockScissorPaper();
            break;
        default:
            Console.WriteLine("BYE BYE !");
            break;
    }
} while (choice < 3);