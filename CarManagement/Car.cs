﻿class Car
{
    public string Make;
    public string Model;
    public int Year;
    public string Color;
    public static Car[] cars = new Car[10];
    public static bool isNewCar = false;
    public Car()
    {
    }
    public Car(string _make, string _model, int _year, string _color)
    {
        Make = _make;
        Model = _model;
        Year = _year;
        Color = _color;
    }

    public void ExistCar()
    {
        cars[0] = new Car();
        cars[0].Make = "Ford";
        cars[0].Model = "Mustang";
        cars[0].Year = 2022;
        cars[0].Color = "White";

        cars[1] = new Car();
        cars[1].Make = "Toyota";
        cars[1].Model = "Camry";
        cars[1].Year = 2021;
        cars[1].Color = "Red";

        cars[2] = new Car();
        cars[2].Make = "Lamborghini";
        cars[2].Model = "Aventador";
        cars[2].Year = 2021;
        cars[2].Color = "Pink";

        cars[3] = new Car();
        cars[3].Make = "Posche";
        cars[3].Model = "Posche 911";
        cars[3].Year = 2021;
        cars[3].Color = "Black";

        cars[4] = new Car();
        cars[4].Make = "Roll Royce";
        cars[4].Model = "Phantom";
        cars[4].Year = 2021;
        cars[4].Color = "Black";

        cars[5] = new Car();
        cars[5].Make = "Mercedes";
        cars[5].Model = "Maybach s680";
        cars[5].Year = 2021;
        cars[5].Color = "Black";

        cars[6] = new Car();
        cars[6].Make = "Mercedes";
        cars[6].Model = "G63 ";
        cars[6].Year = 2021;
        cars[6].Color = "Hien Ho";

        cars[7] = new Car();
        cars[7].Make = "Mercedes";
        cars[7].Model = "Benz ";
        cars[7].Year = 2021;
        cars[7].Color = "Hien Ho";

        cars[8] = new Car();
        cars[8].Make = "Lamborghini";
        cars[8].Model = "Huracan";
        cars[8].Year = 2019;
        cars[8].Color = "Chrome";

        cars[9] = new Car();
        cars[9].Make = "Ferrari";
        cars[9].Model = "F430";
        cars[9].Year = 2019;
        cars[9].Color = "Yellow";
    }
    public void PrintAll()
    {
        foreach (Car car in cars)
        {
            Console.WriteLine("Make: " + car.Make + " | Model: " + car.Model + " | Year: " + car.Year + " |Color: " + car.Color);
        }
    }

    public Car AddNewCar()
    {
        int newElements = cars.Length + 1;
        Array.Resize(ref cars, newElements);
        Car newCar = new Car();
        Console.WriteLine("Enter Car Make: ");
        newCar.Make = Console.ReadLine();
        Console.WriteLine("Enter Car Model: ");
        newCar.Model = Console.ReadLine();
        Console.WriteLine("Enter Make Year: ");
        newCar.Year = int.Parse(Console.ReadLine());
        Console.WriteLine("Enter Car Color: ");
        newCar.Color = Console.ReadLine();
        PrintCarInfo(newCar);
        return cars[newElements - 1] = newCar;
    }

    public void PrintCarInfo(Car newCar)
    {
        Console.WriteLine("Make: " + newCar.Make + " | Model: " + newCar.Model + " | Year: " + newCar.Year + " |Color: " + newCar.Color);
    }
}