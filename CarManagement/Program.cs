﻿int choice = 0;
Car car = new Car();
void Menu()
{
    Console.WriteLine("--------Menu--------");
    Console.WriteLine("| 1: Print all car |");
    Console.WriteLine("| 2: Add new car   |");
    Console.WriteLine("| 3: Exit          |");
    Console.WriteLine("--------------------");
}

do
{
    car.ExistCar();
    Menu();
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            car.PrintAll();
            break;
        case 2:
            car.AddNewCar();
            break;
        default:
            Console.WriteLine("BYE BYE !");
            break;
    }
} while (choice < 3);